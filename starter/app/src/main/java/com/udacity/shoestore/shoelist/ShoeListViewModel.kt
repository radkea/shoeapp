package com.udacity.shoestore.shoelist

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.udacity.shoestore.Repository
import com.udacity.shoestore.models.Shoe

class ShoeListViewModel: ViewModel() {

    private val _shoeObjects = Repository.shoeObjects
    val shoeObjects: LiveData<MutableList<Shoe>>
        get()=_shoeObjects

}