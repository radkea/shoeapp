package com.udacity.shoestore.detail

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.udacity.shoestore.R
import com.udacity.shoestore.databinding.DetailFragmentBinding

class DetailFragment : Fragment() {

    private lateinit var viewModel: DetailViewModel
    private lateinit var binding: DetailFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.detail_fragment, container, false)

        viewModel = ViewModelProvider(this).get(DetailViewModel::class.java)

        binding.detailViewModel = viewModel
        binding.setLifecycleOwner(this)

        viewModel.eventGoBack.observe(viewLifecycleOwner, Observer { done->
            if(done)
                findNavController().popBackStack()
        })

        return binding.root
    }
}