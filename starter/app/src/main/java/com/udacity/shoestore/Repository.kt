package com.udacity.shoestore

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.udacity.shoestore.models.Shoe

object Repository {

    private val _shoe = MutableLiveData<Shoe>()

    private val _shoeObjects = MutableLiveData<MutableList<Shoe>>()
    val shoeObjects: LiveData<MutableList<Shoe>>
        get()= _shoeObjects

    init{
        //populate list with a dummy shoe
        _shoeObjects.value = mutableListOf(Shoe("FlipFlops", "19.5", "BigFoot", "BigFoot FlipFlops"))
    }

    fun addShoe(shoe: Shoe){
        _shoeObjects.value?.add(shoe)
    }
}