package com.udacity.shoestore.shoelist

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.udacity.shoestore.R
import com.udacity.shoestore.databinding.ShoeListFragmentBinding
import com.udacity.shoestore.databinding.ShoeListItemBinding

class ShoeListFragment : Fragment() {

    private lateinit var viewModel: ShoeListViewModel
    private lateinit var binding: ShoeListFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.shoe_list_fragment, container, false)

        setHasOptionsMenu(true)

        binding = DataBindingUtil.inflate(inflater, R.layout.shoe_list_fragment, container, false)
        viewModel = ViewModelProvider(this).get(ShoeListViewModel::class.java)
        binding.shoeListViewModel = viewModel
        binding.setLifecycleOwner(this)


        viewModel.shoeObjects.observe(viewLifecycleOwner, Observer { listOfShoes->
            listOfShoes.forEach{

                //borrowed from knowledge.udacity.com/questions/393644

//                DataBindingUtil.inflate<ListItemShoeBinding>(layoutInflater, R.layout.list_item_shoe, container, true)
//                val tv = TextView(this.context)
//                val textView = TextView(this.context)
//                textView.text = content

                val itemBinding:ShoeListItemBinding = DataBindingUtil.inflate(layoutInflater, R.layout.shoe_list_item,
                    container, false)

                val content = "Shoe Name= ${it.name.toString()} \nCompany Name= ${it.company.toString()} \n"+
                        "Shoe Size= ${it.size.toString()} \nDescription= ${it.description.toString()}\n"

                itemBinding.shoeScrollViewDetails = content

                binding.linearLayoutScroll.addView(itemBinding.root)

            }
        })

        binding.fab.setOnClickListener { View ->
            findNavController().navigate(ShoeListFragmentDirections.actionShoeListFragmentToDetailFragment())
        }

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.overflow_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item, requireView().findNavController())
                || super.onOptionsItemSelected(item)
    }
}