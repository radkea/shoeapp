package com.udacity.shoestore.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.udacity.shoestore.Repository
import com.udacity.shoestore.models.Shoe

class DetailViewModel : ViewModel() {

    val editTextName = MutableLiveData<String>()
    val editTextCompanyName = MutableLiveData<String>()
    val editTextShoeSize = MutableLiveData<String>()
    val editTextDescription = MutableLiveData<String>()

    private val _eventGoBack = MutableLiveData<Boolean>()
    val eventGoBack: LiveData<Boolean>
        get()= _eventGoBack

    init {
        _eventGoBack.value = false
    }

    fun saveButton() {
        val shoe = Shoe(name = editTextName.value.toString(), size = editTextShoeSize.value.toString(),
            company = editTextCompanyName.value.toString(), description =  editTextDescription.value.toString())

        Repository.addShoe(shoe)

        _eventGoBack.value = true
    }

    fun cancelButton() {
        _eventGoBack.value = true
    }
}